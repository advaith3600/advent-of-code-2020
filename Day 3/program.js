const fs = require('fs')
fs.readFile('input.txt', 'utf8', (err, data) => {
    if (err)
        return console.error(err);

    let count = 0;
    data.split('\n').forEach((element, index) => {
        element = element.trim()
        if (element[(index * 3) % element.length] === '#') count++;
    })

    console.log(count);
})