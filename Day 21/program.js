const fs = require('fs')

const ingredients = new Set(),
    allergens = {},
    lists = fs.readFileSync('input.txt', 'utf-8')
        .split('\r\n')
        .map(elem => {
            const values = {
                ingredients: elem.split(/ \(contains [a-zA-Z,\s)]+|\s/).map(u => u.trim()),
                allergens: elem.split(/^[a-zA-Z\s]+\(contains\s|[,)]/).map(u => u.trim())
            }
            values.ingredients.pop()
            values.allergens.pop()
            values.allergens.shift()

            values.ingredients.forEach(ingredient => ingredients.add(ingredient))
            return values
        });

for (let list of lists)
    for (let allergen of list.allergens)
        allergens[allergen] = new Set(ingredients);

for (let [allergen, ingredients] of Object.entries(allergens)) {
    for (let list of lists) {
        if (!list.allergens.includes(allergen)) continue;

        for (let ingredient of ingredients)
            if (!list.ingredients.includes(ingredient))
                ingredients.delete(ingredient);
    }
}

let safeIngredients = new Set(ingredients)
for (let ingredients of Object.values(allergens))
    for (let ingredient of ingredients)
        safeIngredients.delete(ingredient)

let safeIngredientsCount = 0;
for (let list of lists)
    for (let ingredient of list.ingredients)
        safeIngredientsCount += safeIngredients.has(ingredient)

console.log(safeIngredientsCount)