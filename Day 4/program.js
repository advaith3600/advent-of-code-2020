const fs = require('fs')
fs.readFile('input.txt', 'utf8', (err, data) => {
    if (err)
        return console.error(err);

    let count = 0;
    data = data.split('\r\n\r\n')
    data.forEach(credentials => {
        credentials = credentials.split(/[\s\n]+/)
        const constraints = {
            byr: false,
            iyr: false,
            eyr: false,
            hgt: false,
            hcl: false,
            ecl: false,
            pid: false
        }

        credentials.forEach(prop => constraints[prop.split(':')[0]] = true)
        if (constraints.byr && constraints.iyr &&
            constraints.eyr && constraints.hgt &&
            constraints.hcl && constraints.ecl && constraints.pid) count++;
    })

    console.log(count);
})