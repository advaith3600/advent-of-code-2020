fs = require('fs')
fs.readFile('input.txt', 'utf8', (err, data) => {
    if (err)
        return console.error(err);

    data = data.split('\n')
    let result = 0;
    for (let i = 0; i < data.length; i++) 
        for (let j = 0; j < data.length; j++) 
            if (i !== j && Number(data[i]) + Number(data[j]) === 2020)
                result = Number(data[i]) * Number(data[j]);

    console.log(result)
});