const fs = require('fs')

let count = 0;
fs.readFileSync('input.txt', 'utf-8')
    .split('\r\n\r\n')
    .forEach(element => {
        count += element.split(/(?=\w)|[\r\n]+/)
            .reduce((acc, cur) => acc.includes(cur) ? acc : [...acc, cur], [])
            .length
    })

console.log(count);