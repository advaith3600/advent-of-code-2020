const fs = require('fs')
const divide = (lower, upper, key) => {
    const diff = upper - lower;
    if (key === 'F' || key === 'L') // lower half
        return [lower, Math.floor(upper - diff / 2)];
    else if (key === 'B' || key === 'R') // upper half
        return [Math.floor(lower + diff / 2), upper];
};

let max = 0;
fs.readFileSync('input.txt', 'utf8')
    .split('\n')
    .forEach(pass => {
        const moves = pass.trim().split('')

        // finding row
        let lower = 0, upper = 127;
        for (let i = 0; i < 7; i++)
            [lower, upper] = divide(lower, upper, moves[i]);
        const row = upper;

        // finding column
        lower = 0;
        upper = 7;
        for (let i = 0; i < 3; i++)
            [lower, upper] = divide(lower, upper, moves[7 + i]);
        const column = upper;

        max = Math.max(max, row * 8 + column);
    })

console.log(max)
