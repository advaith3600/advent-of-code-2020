fs = require('fs')
fs.readFile('input.txt', 'utf8', (err, data) => {
    if (err)
        return console.error(err);

    let count = 0;
    data.split('\n').forEach(element => {
        const [condition, password] = element.split(':')
        const [limits, letter] = condition.split(' ')
        const [lower, upper] = limits.split('-')

        let letterCount = 0
        password.split('').forEach(elm => (elm === letter) ? letterCount++ : '')
        if (lower <= letterCount && upper >= letterCount) count++;
    });

    console.log(count)
});